# Genlica

<p align="center">
    <a href="https://gitlab.com/xgqt/genlica/pipelines">
        <img src="https://gitlab.com/xgqt/genlica/badges/master/pipeline.svg">
    </a>
    <a href="https://gitlab.com/xgqt/genlica/commits/master.atom">
        <img src="https://img.shields.io/badge/feed-atom-orange.svg">
    </a>
    <a href="./LICENSE">
        <img src="https://img.shields.io/badge/license-GPLv3-blue.svg">
    </a>
</p>

Gentoo replicator


# About

Those are the setting i find sane for a general use high performance gentoo workstation.


# Dependencies

## Any stow implementation

- stow
- xstow
- pystow


# Installation

## Git

As root:

```sh
mkdir -p /opt/genlica
cd /opt/genlica
git clone --recursive --verbose https://gitlab.com/xgqt/genlica
cd genlica
bash install
```


## Gentoo

As root:

```sh
emerge -1nv app-eselect/eselect-repository
eselect repository add myov git https://gitlab.com/xgqt/myov
emaint sync -r myov
emerge -av --autounmask app-portage/genlica
```


# Explanation

## test.sh

The [test.sh](./test.sh) script has a .sh extension to make it stand out,
also it will be removed while installing [genlica](https://gitlab.com/xgqt/genlica) from [myov](https://gitlab.com/xgqt/myov) overlay.


# License

SPDX-License-Identifier: GPL-3.0-only & CC0-1.0

Scripts with the GPL header in this repository are licensed under the appropriate license (GPL-3.0-only).

The configuration files (.conf) are licensed under the CC0 1.0 Universal license.

## Unless otherwise stated contents here are under the GNU GPL v3 license

This file is part of genlica.

genlica is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, version 3.

genlica is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with genlica.  If not, see <https://www.gnu.org/licenses/>.

Copyright (c) 2019-2021, Maciej Barć <xgqt@riseup.net>
Licensed under the GNU GPL v3 License
