#!/usr/bin/env bash


# This file is part of genlica.

# genlica is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, version 3.

# genlica is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with genlica.  If not, see <https://www.gnu.org/licenses/>.

# Copyright (c) 2021, Maciej Barć <xgqt@riseup.net>
# Licensed under the GNU GPL v3 License


trap 'exit 128' INT
export PATH
set -e


my_title="$(basename "${0#*-}" | sed 's/\.sh//g')"
my_pre="[${my_title^^}]:"

name="${1}"
uris="${2}"
path="${3}"

ncpu=$(nproc || echo "2")


if [ -n "${name}" ] && [ -d "${path}/.git" ]
then
    echo ">>> Running hook: ${my_title}"
    echo "${my_pre} For repository:  ${name}"
    echo "${my_pre} With remote:     ${uris}"
    echo "${my_pre} Into directory:  ${path}"

    egencache --jobs="${ncpu}" --load-average="${ncpu}"  \
              --repo="${name}" --update

    # Do not check since we use "set -e"
    echo "${my_pre} Finished successfully"
fi
