#!/usr/bin/env bash


# This file is part of genlica.

# genlica is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, version 3.

# genlica is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with genlica.  If not, see <https://www.gnu.org/licenses/>.

# Copyright (c) 2021, Maciej Barć <xgqt@riseup.net>
# Licensed under the GNU GPL v3 License

# Inspired by 'demerge'.


trap 'exit 128' INT
export PATH


my_title="$(basename "${0#*-}" | sed 's/\.sh//g')"
my_pre="[${my_title^^}]:"


echo ">>> Running hook: ${my_title}"

# If we don't have git just exit success
type git  >/dev/null 2>&1  || exit 0

my_portage="${EPREFIX}/etc/portage"

pushd "${my_portage}"  >/dev/null 2>&1  || exit 1


if [ ! -d "${my_portage}"/.git ]
then
    echo "${my_pre} Initializing a git repository in ${my_portage} ..."

    git init  || exit 1

    git config --get user.email  >/dev/null 2>&1  ||
        git config --local user.email "portageadmin@localhost"  ||
        exit 1
    git config --get user.name   >/dev/null 2>&1  ||
        git config --local user.name "Portage Admin"  ||
        exit 1
fi


git add .

if git diff-index --quiet HEAD --
then
    echo "${my_pre} Nothing to commit"
else
    echo "${my_pre} Creating a commit ..."

    git commit -m "automatic update via ${my_title} hook"  || exit 1
fi


echo "${my_pre} Collecting garbage ..."
git gc --aggressive  || exit 0


popd  >/dev/null 2>&1  || exit 0
echo "${my_pre} Finished successfully"
