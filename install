#!/bin/sh


# This file is part of genlica.

# genlica is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, version 3.

# genlica is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with genlica.  If not, see <https://www.gnu.org/licenses/>.

# Copyright (c) 2019-2021, Maciej Barć <xgqt@riseup.net>
# Licensed under the GNU GPL v3 License

# Install Genlica


trap 'exit 128' INT
export PATH


main() {
    cd "$(dirname "$(readlink -f "${0}")")" || exit 1

    # Auto-select the stow implementation
    if command -v stow >/dev/null 2>&1
    then
        stow_impl=stow
    elif command -v xstow >/dev/null 2>&1
    then
        stow_impl=xstow
    elif command -v pystow >/dev/null 2>&1
    then
        stow_impl=pystow
    else
        echo "[ERROR]: There is no stow on the system!"
        echo "[ERROR]: Existing with failure!"
        exit 1
    fi

    # Back up the old portage directory
    if [ -d "${EPREFIX}/etc/portage" ] &&
           [ ! -d "${EPREFIX}/etc/portage.bak" ]
    then
        mv "${EPREFIX}/etc/portage" "${EPREFIX}/etc/portage.bak"  \
            || exit 1
    fi

    # Reconstruct portage directory
    mkdir -p "${EPREFIX}/etc/portage/env"
    mkdir -p "${EPREFIX}/etc/portage/make.conf"
    mkdir -p "${EPREFIX}/etc/portage/package.accept_keywords"
    mkdir -p "${EPREFIX}/etc/portage/package.env"
    mkdir -p "${EPREFIX}/etc/portage/package.mask"
    mkdir -p "${EPREFIX}/etc/portage/package.use"
    mkdir -p "${EPREFIX}/etc/portage/postsync.d"
    mkdir -p "${EPREFIX}/etc/portage/repo.postsync.d"
    mkdir -p "${EPREFIX}/etc/portage/repos.conf"
    mkdir -p "${EPREFIX}/etc/portage/savedconfig"
    mkdir -p "${EPREFIX}/etc/portage/sets"

    # Install genlica
    ${stow_impl} -v portage -t "${EPREFIX}/etc/portage"  \
        || exit 1

    # Select the profile
    if [ -z "${EPREFIX}" ]
    then
        eselect profile set default/linux/amd64/17.1
    fi

    # Create basic addition to 99-main
    if [ ! -e "${EPREFIX}/etc/portage/make.conf/00-x86.conf" ]
    then
        cat > "${EPREFIX}/etc/portage/make.conf/00-x86.conf" <<EOF
CPU_FLAGS_X86="$(cpuid2cpuflags | awk -F : '{print $2}')"
EOF
    fi
    if [ ! -e "${EPREFIX}/etc/portage/make.conf/01-makeopts.conf" ]
    then
        cat > "${EPREFIX}/etc/portage/make.conf/01-makeopts.conf" <<EOF
MAKEOPTS="--jobs=$(nproc) --load-average=$(nproc)"
EOF
    fi
    if [ ! -e "${EPREFIX}/etc/portage/make.conf/02-flags.conf" ]
    then
        cat > "${EPREFIX}/etc/portage/make.conf/02-flags.conf" <<EOF
COMMON_FLAGS="-march=native -O2 -pipe"
EOF
    fi
    if [ ! -e "${EPREFIX}/etc/portage/make.conf/03-emerge.conf" ]
    then
        cat > "${EPREFIX}/etc/portage/make.conf/03-emerge.conf" <<EOF
EMERGE_DEFAULT_OPTS="\${EMERGE_DEFAULT_OPTS} --jobs=$(nproc)"
EOF
    fi

    repos="${EPREFIX}/var/db/repos"
    for repo in "${repos}/gentoo" "${repos}/localrepo" "${repos}/myov"
    do
        [ -e "${repo}" ] && rm -r "${repo}"
        mkdir -p "${repo}"
    done

    mkdir -p "${EPREFIX}/var/db/repos/localrepo/profiles"
    mkdir -p "${EPREFIX}/var/db/repos/localrepo/metadata"
    cat > "${EPREFIX}/var/db/repos/localrepo/metadata/layout.conf" <<EOF
masters = gentoo

cache-formats = md5-dict
manifest-hashes = BLAKE2B
manifest-required-hashes = BLAKE2B
sign-commits = true
sign-manifests = false
thin-manifests = true
update-changelog = false

eapis-banned = 0 1 2 3 4 5 6
EOF
    cat > "${EPREFIX}/var/db/repos/localrepo/profiles/repo_name" <<EOF
localrepo
EOF

    # Setup the gentoo repository
    # Edit this afterwards if you are on Eprefix or Funtoo
    cat > "${EPREFIX}/etc/portage/repos.conf/gentoo.conf" <<EOF
[DEFAULT]
main-repo = gentoo

[gentoo]
auto-sync = yes
location = /var/db/repos/gentoo
sync-type = git
sync-umask = 022
sync-uri = https://github.com/gentoo-mirror/gentoo.git
sync-user = root:portage
EOF
    # Add myov too
    cat > "${EPREFIX}/etc/portage/repos.conf/myov.conf" <<EOF
[myov]
auto-sync = yes
location = /var/db/repos/myov
sync-git-clone-extra-opts = --depth=999999999 --no-shallow-submodules --no-single-branch
sync-git-pull-extra-opts = --depth=999999999
sync-type = git
sync-umask = 022
sync-uri = https://gitlab.com/xgqt/myov.git
sync-user = root:portage
EOF
    # And localrepo
    cat > "${EPREFIX}/etc/portage/repos.conf/localrepo.conf" <<EOF
[localrepo]
auto-sync = no
location = /var/db/repos/localrepo
EOF

    # Add notmp
    if [ -x ./create_notmp ] && [ -z "${EPREFIX}" ]
    then
        sh create_notmp
    fi

    # Synchronize repositories
    emaint sync -r gentoo
    emaint sync -r myov

    # (Do not) read the news
    eselect news read new  >/dev/null 2>&1
}


if [ "$(whoami)" != root ] && [ -z "${EPREFIX}" ]
then
    sudo "${0}" "${@}"
else
    main
fi
